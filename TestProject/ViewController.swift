//
//  ViewController.swift
//  TestProject
//
//  Created by Dmitry Protasov on 12/27/16.
//  Copyright © 2016 Turvo. All rights reserved.
//

import UIKit
import GoogleMaps
import Cosmos

class ViewController: UIViewController {
    fileprivate static let ZOOM_LEVEL: Float = 15
    fileprivate static let CARDVIEW_UP_CONSTRAINT: CGFloat = 80
    
    fileprivate var locationManager: CLLocationManager = CLLocationManager()
    fileprivate var isContainerUpped = false
    fileprivate var menuButton: HamburgerButton?
    
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var cardViewTopConstr: NSLayoutConstraint!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var lblMessages: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initLocationManager()
        initMap()
        
        self.menuButton = HamburgerButton(frame: CGRect(x: 12, y: 24, width: 32, height: 32))
        if let menuButton = self.menuButton {
            menuButton.closeColor = UIColor.white
            menuButton.menuColor = UIColor.black
            menuButton.addTarget(self, action: #selector(didMenuButtonPressed(_:)), for:.touchUpInside)
            self.view.addSubview(menuButton)
        }
        
        imgPhoto.layer.cornerRadius = imgPhoto.bounds.height / 2
        imgPhoto.layer.masksToBounds = true
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(cardViewDragged(gesture:)))
        gesture.delegate = self
        cardView.addGestureRecognizer(gesture)
        cardView.isUserInteractionEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        closeDetailsView(view: cardView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func initLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
    }
    
    private func initMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 53.888378, longitude: 27.544535, zoom: ViewController.ZOOM_LEVEL)
        self.mapView?.isMyLocationEnabled = true
        self.mapView?.settings.myLocationButton = false
        self.mapView.moveCamera(GMSCameraUpdate.setCamera(camera))
    }
    
    private func openDetailsView(view: UIView) {
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.transparentView.backgroundColor = UIColor.black
            view.frame.origin.y = ViewController.CARDVIEW_UP_CONSTRAINT
            self?.lblMessages.alpha = 1
            }, completion: { [weak self] _ in
                self?.cardViewTopConstr.constant = view.frame.origin.y
        })
        self.menuButton?.showsMenu = true
    }
    
    private func closeDetailsView(view: UIView) {
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.transparentView.backgroundColor = UIColor.clear
            if let transparentView = self?.transparentView {
                view.frame.origin.y = transparentView.frame.height - (view.frame.height - 2)
            }
            self?.lblMessages.alpha = 0
            }, completion: { [weak self] _ in
                self?.cardViewTopConstr.constant = view.frame.origin.y
        })
        self.menuButton?.showsMenu = false
    }
    
    func cardViewDragged(gesture: UIPanGestureRecognizer) {
        if let gestureView = gesture.view, gesture.state == .began || gesture.state == .changed {
            let translation = gesture.translation(in: self.view)
            
            if gestureView.frame.origin.y + (gestureView.frame.height - 2) <= UIScreen.main.bounds.height {
                gestureView.center = CGPoint(x: gestureView.center.x, y: gestureView.center.y + translation.y)
            }
            gesture.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
            
            let alpha = 1 - ((gestureView.frame.origin.y - ViewController.CARDVIEW_UP_CONSTRAINT) / (self.transparentView.frame.height - (gestureView.frame.height - 2)))
            transparentView.backgroundColor = UIColor.black.withAlphaComponent(alpha)
            self.lblMessages.alpha = alpha
            self.view.layoutIfNeeded()
        }
        
        if let gestureView = gesture.view, gesture.state == .ended {
            if gestureView.center.y < (self.transparentView.frame.height + ViewController.CARDVIEW_UP_CONSTRAINT) / 2 {
                openDetailsView(view: gestureView)
            } else {
                closeDetailsView(view: gestureView)
            }
        }
    }
    
    func didMenuButtonPressed(_ sender: AnyObject!) {
        if let menuButton = self.menuButton {
            if menuButton.showsMenu {
                closeDetailsView(view: self.cardView)
            } else {
                openDetailsView(view: self.cardView)
            }
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let locationUpdate = GMSCameraUpdate.setTarget(location.coordinate)
            self.mapView?.animate(with: locationUpdate)
        }
    }
}

extension ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view is CosmosView {
            return false
        }
        return true
    }
}

