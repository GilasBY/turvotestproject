import CoreGraphics
import QuartzCore
import UIKit

@IBDesignable class HamburgerButton : UIButton {
    
    @IBInspectable var menuColor: UIColor = UIColor.white
    @IBInspectable var closeColor: UIColor = UIColor.white
    @IBInspectable var size: CGFloat = 0
    @IBInspectable var menu: Bool = false
    
    var scaleFactor: CGFloat = 1.0
    var strokeSize: CGFloat {
        get { return 4 * scaleFactor }
    }
    
    let menuStrokeStart: CGFloat = 0.325
    let menuStrokeEnd: CGFloat = 0.95
    
    let hamburgerStrokeStart: CGFloat = 0.0263
    let hamburgerStrokeEnd: CGFloat = 0.1126
    
    var drawingRect: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override public var intrinsicContentSize: CGSize {
        return size > 0 ? CGSize(width: size, height: size) : CGSize(width: 44.0, height: 44.0)
    }
    
    override func draw(_ rect: CGRect) {
        scaleFactor = (size > 0) ? size / 54.0 : (frame.height < frame.width) ? frame.height / 54.0 : frame.width / 54.0
        
        let outline: CGPath = {
            let path = CGMutablePath()
            path.move(to: CGPoint(x: 10 * self.scaleFactor, y: 27 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 40 * self.scaleFactor, y: 27 * self.scaleFactor), control1: CGPoint(x: 12 * self.scaleFactor, y: 27 * self.scaleFactor), control2: CGPoint(x: 28.02 * self.scaleFactor, y: 27 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 27 * self.scaleFactor, y: 02 * self.scaleFactor), control1: CGPoint(x: 55.92 * self.scaleFactor, y: 27 * self.scaleFactor), control2: CGPoint(x: 50.47 * self.scaleFactor, y: 2 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 2 * self.scaleFactor, y: 27 * self.scaleFactor), control1: CGPoint(x: 13.16 * self.scaleFactor, y: 2 * self.scaleFactor), control2: CGPoint(x: 2 * self.scaleFactor, y: 13.16 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 27 * self.scaleFactor, y: 52 * self.scaleFactor), control1: CGPoint(x: 2 * self.scaleFactor, y: 40.84 * self.scaleFactor), control2: CGPoint(x: 13.16 * self.scaleFactor, y: 52 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 52 * self.scaleFactor, y: 27 * self.scaleFactor), control1: CGPoint(x: 40.84 * self.scaleFactor, y: 52 * self.scaleFactor), control2: CGPoint(x: 52 * self.scaleFactor, y: 40.84 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 27 * self.scaleFactor, y: 2 * self.scaleFactor), control1: CGPoint(x: 52 * self.scaleFactor, y: 13.16 * self.scaleFactor), control2: CGPoint(x: 42.39 * self.scaleFactor, y: 2 * self.scaleFactor))
            path.addCurve(to: CGPoint(x: 2 * self.scaleFactor, y: 27 * self.scaleFactor), control1: CGPoint(x: 13.16 * self.scaleFactor, y: 2 * self.scaleFactor), control2: CGPoint(x: 2 * self.scaleFactor, y: 13.16 * self.scaleFactor))
            
            return path
        }()
        
        let shortStroke: CGPath = {
            let path = CGMutablePath()
            path.move(to: CGPoint(x: 2 * self.scaleFactor, y: 2))
            path.addLine(to: CGPoint(x: 28 * self.scaleFactor, y:2))
            
            return path
        }()
        
        self.top.path = shortStroke
        self.middle.path = outline
        self.bottom.path = shortStroke
        
        for layer in [ self.top, self.middle, self.bottom ] {
            layer?.fillColor = nil
            layer?.strokeColor = menuColor.cgColor
            layer?.lineWidth = strokeSize
            layer?.miterLimit = 100
            layer?.lineCap = kCALineCapRound
            layer?.masksToBounds = true
            
            let strokingPath = CGPath(__byStroking: (layer?.path!)!, transform: nil, lineWidth: 4, lineCap: .round, lineJoin: .miter, miterLimit: 100)
            
            layer?.bounds = (strokingPath?.boundingBoxOfPath)!
            
            layer?.actions = [
                "strokeStart": NSNull(),
                "strokeEnd": NSNull(),
                "transform": NSNull()
            ]
            
            self.layer.addSublayer(layer!)
        }
        
        self.top.anchorPoint = CGPoint(x: 28.0 / 30.0, y: 0.5)
        self.top.position = CGPoint(x: (frame.width / 2) + (13.45 * scaleFactor), y: (frame.height / 2) - (9.5 * scaleFactor))
        self.top.transform = (menu) ? CATransform3DRotate(CATransform3DMakeTranslation(-4 * scaleFactor, 0, 0), -0.7853975, 0, 0, 1) : CATransform3DIdentity
        
        self.middle.position = CGPoint(x: frame.width / 2.0, y: frame.height / 2.0)
        self.middle.strokeStart = (menu) ? menuStrokeStart : hamburgerStrokeStart
        self.middle.strokeEnd = (menu) ? menuStrokeEnd : hamburgerStrokeEnd
        
        self.bottom.anchorPoint = CGPoint(x: 28.0 / 30.0, y: 0.5 )
        self.bottom.position = CGPoint(x: (frame.width / 2) + (13.45 * scaleFactor), y: (frame.height / 2) + (9.5 * scaleFactor))
        self.bottom.transform = (menu) ? CATransform3DRotate(CATransform3DMakeTranslation(-4 * scaleFactor, 0, 0), 0.7853975, 0, 0, 1) : CATransform3DIdentity
        
        drawingRect = true
        showsMenu = menu
        drawingRect = false
    }
    
    var showsMenu: Bool = false {
        didSet {
            if drawingRect { return }
            
            let strokeStart = CABasicAnimation(keyPath: "strokeStart")
            let strokeEnd = CABasicAnimation(keyPath: "strokeEnd")
            let strokeColor = CABasicAnimation(keyPath: "strokeColor")
            
            if self.showsMenu {
                strokeStart.toValue = menuStrokeStart
                strokeStart.duration = 0.5
                strokeStart.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, -0.4, 0.5, 1)
                
                strokeEnd.toValue = menuStrokeEnd
                strokeEnd.duration = 0.6
                strokeEnd.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, -0.4, 0.5, 1)
                
                strokeColor.toValue = closeColor.cgColor
                strokeEnd.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, -0.4, 0.5, 1)
            } else {
                strokeStart.toValue = hamburgerStrokeStart
                strokeStart.duration = 0.5
                strokeStart.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, 0, 0.5, 1.2)
                strokeStart.beginTime = CACurrentMediaTime() + 0.1
                strokeStart.fillMode = kCAFillModeBackwards
                
                strokeEnd.toValue = hamburgerStrokeEnd
                strokeEnd.duration = 0.6
                strokeEnd.timingFunction = CAMediaTimingFunction(controlPoints: 0.25, 0.3, 0.5, 0.9)
                
                strokeColor.toValue = menuColor.cgColor
            }
            
            
            self.middle.ocb_applyAnimation(animation: strokeColor)
            self.middle.ocb_applyAnimation(animation: strokeStart)
            self.middle.ocb_applyAnimation(animation: strokeEnd)
            
            
            let topTransform = CABasicAnimation(keyPath: "transform")
            topTransform.timingFunction = CAMediaTimingFunction(controlPoints: 0.5, -0.8, 0.5, 1.85)
            topTransform.duration = 0.4
            topTransform.fillMode = kCAFillModeBackwards
            
            let bottomTransform = topTransform.copy() as! CABasicAnimation
            
            if self.showsMenu {
                let translation = CATransform3DMakeTranslation(-4 * scaleFactor, 0, 0)
                
                topTransform.toValue = NSValue(caTransform3D: CATransform3DRotate(translation, -0.7853975, 0, 0, 1))
                topTransform.beginTime = CACurrentMediaTime() + 0.25
                
                bottomTransform.toValue = NSValue(caTransform3D: CATransform3DRotate(translation, 0.7853975, 0, 0, 1))
                bottomTransform.beginTime = CACurrentMediaTime() + 0.25
            } else {
                topTransform.toValue = NSValue(caTransform3D: CATransform3DIdentity)
                topTransform.beginTime = CACurrentMediaTime() + 0.05
                
                bottomTransform.toValue = NSValue(caTransform3D: CATransform3DIdentity)
                bottomTransform.beginTime = CACurrentMediaTime() + 0.05
            }
            
            self.top.ocb_applyAnimation(animation: topTransform)
            self.top.ocb_applyAnimation(animation: strokeColor)
            self.bottom.ocb_applyAnimation(animation: bottomTransform)
            self.bottom.ocb_applyAnimation(animation: strokeColor)
        }
    }
    
    var top: CAShapeLayer! = CAShapeLayer()
    var bottom: CAShapeLayer! = CAShapeLayer()
    var middle: CAShapeLayer! = CAShapeLayer()
}

extension CALayer {
    func ocb_applyAnimation(animation: CABasicAnimation) {
        let copy = animation.copy() as! CABasicAnimation
        
        if !(copy.fromValue != nil) {
            copy.fromValue = self.presentation()?.value(forKeyPath: copy.keyPath!)
        }
        
        self.add(copy, forKey: copy.keyPath)
        self.setValue(copy.toValue, forKeyPath:copy.keyPath!)
    }
}
